<!DOCTYPE html>
<html>
    <head>
        <title>Lab 9</title>
        <meta charset="utf-8">
    </head>
    <body>
        <header><h1>Respuesta Problema 5</h1></header>
        <?php
            $ar= array($_GET["comp1"],$_GET["comp2"],$_GET["comp3"]);
            $biggerthan= array(array(0,0,0),array(0,0,0),array(0,0,0));
            if($ar[0]==">"){
                $biggerthan[0][1]=1;
            }
            else{
                $biggerthan[1][0]=1;
            }
            if($ar[1]==">"){
                $biggerthan[1][2]=1;
            }
            else{
                $biggerthan[2][1]=1;
            }
            if($ar[2]==">"){
                $biggerthan[0][2]=1;
            }
            else{
                $biggerthan[2][0]=1;
            }
            $flag=false;
            $num1;
            $num2;
            $num3;
            for($i=0;$i<3;$i++){
                $sumar=0;
                for($j=0;$j<3;$j++){
                    $sumar+=$biggerthan[$i][$j];
                }
                if($sumar==2){
                    $flag=true;
                    if($i==0){
                        $num1="A";
                    }
                    else if($i==1){
                        $num1="B";
                    }
                    else{
                        $num1="C";
                    }
                    $i=3;
                }
            }
            for($i=0;$i<3;$i++){
                $sumar=0;
                for($j=0;$j<3;$j++){
                    $sumar+=$biggerthan[$i][$j];
                }
                if($sumar==1){
                    if($i==0){
                        $num2="A";
                    }
                    else if($i==1){
                        $num2="B";
                    }
                    else{
                        $num2="C";
                    }
                    $i=3;
                }
            }
            for($i=0;$i<3;$i++){
                $sumar=0;
                for($j=0;$j<3;$j++){
                    $sumar+=$biggerthan[$i][$j];
                }
                if($sumar==0){
                    if($i==0){
                        $num3="A";
                    }
                    else if($i==1){
                        $num3="B";
                    }
                    else{
                        $num3="C";
                    }
                    $i=3;
                }
            }
            if($flag==false){
                echo "Configuración imposible";
            }
            else{
                echo "$num3 $num2 $num1";
            }
        ?>
    </body>
</html>