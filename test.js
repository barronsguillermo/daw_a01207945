let ev=prompt("Escribe un número a tabular: ");
document.write("<br><strong>Ejercicio 1</strong><br>");
document.write("<table>");
document.write("<thead>");
document.write("<tr>");
document.write("<td>");
document.write("Número");
document.write("</td>");
document.write("<td>");
document.write("Cuadrado");
document.write("</td>");
document.write("<td>");
document.write("Cubo");
document.write("</td>");
document.write("</tr>");
document.write("<thead>");
document.write("<tbody>");
for(let i=1;i<=ev;i++){
    document.write("<tr>");
    document.write("<td>");
    document.write(i);
    document.write("</td>");
    document.write("<td>");
    document.write(i*i);
    document.write("</td>");
    document.write("<td>");
    document.write(i*i*i);
    document.write("</td>");
    document.write("</tr>");
}
document.write("</tbody>");
document.write("</table>");
document.write("<br><strong>Ejercicio 2</strong><br>");
let num1=Math.floor(Math.random()*300);
let num2=Math.floor(Math.random()*300);
let dis="Escribe la suma de: "+num1+" + "+num2;
let prev=new Date();
let comp=prompt(dis);
let aft=new Date();
let ti=aft-prev;
if(comp==num1+num2){
    alert("Resultado correcto.");
}
else{
    alert("Resultado incorrecto");
}
document.write("Tardaste "+ti+" milisegundos en responder");
let arreglo=prompt("Escribe un arreglo de números, separados solo por espacios");
document.write("<br><strong>Ejercicio 3</strong><br>");
function contador (){
    let negativos=0;
    let positivos=0;
    let ceros=0;
    let cur =0;
    let flag=false;
    for(let i=0;i<arreglo.length;i++){
        if(arreglo.charAt(i)>='0'&&arreglo.charAt(i)<='9'){
            cur*=10;
            cur+=arreglo.charAt(i);
        }
        else if(arreglo.charAt(i)=='-'){
            flag=true;
        }
        else{
            if(cur==0){
                ceros++;
            }
            else if(cur>0&&flag==false){
                positivos++;
            }
            else{
                negativos++;
            }
            cur=0;
            flag=false;
        }
    }
    if(cur==0){
        ceros++;
    }
    else if(cur>0&&flag==false){
        positivos++;
    }
    else{
        negativos++;
    }
    document.write("<br>");
    document.write("Positivos: "+positivos);
    document.write("<br>");
    document.write("\nNegativos: "+negativos);
    document.write("<br>");
    document.write("\nCeros: "+ceros);    
}
contador();
document.write("<br><strong>Ejercicio 4</strong><br>");
function promedios(){
    let rows=prompt("Escribe el número de filas: ");
    let columns=prompt("Escribe el número de columnas: ");
    for(let j=0;j<rows;j++){
        let arreglo=prompt("Escribe los " +columns+" números de la fila (separados por un solo espacio): ");
        let cur = 0;
        let flag=false;
        let sumanum=0;
        for(let i=0;i<arreglo.length;i++){
            if(arreglo.charAt(i)>='0'&&arreglo.charAt(i)<='9'){
                cur*=10;
                cur+=Number(arreglo.charAt(i));
            }
            else if(arreglo.charAt(i)=='-'){
                flag=true;
            }
            else{
                if(flag==true){
                    cur*=(-1);
                }
                sumanum+=cur;
                cur=0;
                flag=false;
            }
        }
        sumanum+=cur;
        document.write("<br>");
        document.write("El promedio de la fila "+(j+1)+" es: "+(sumanum/columns));
        sumanum=0;
    }
}
promedios();
document.write("<br><strong>Ejercicio 5</strong><br>");
function inversos(){
    let num1=prompt("Escribe un número a invertir: ");
    let invertido=0;
    while(num1>0){
        invertido*=10;
        invertido+=(num1%10);
        num1=Math.floor(num1/10);
    }
    document.write("<br>");
    document.write(invertido);
}
inversos();
document.write("<br><strong>Ejercicio 6</strong><br>");
function calendario(){
    document.write("<br>");
    document.write("Problema tipo ACM (Educational CodeForces Round 13), este programa dice cuál es el siguiente año con el mismo día de la semana de inicio y de fin: ");
    document.write("<br>");
    let y=prompt("Escribe un año entre mil y diez mil:");
    function Year(numval,st){
        this.num=numval;
        this.start=st;
        this.getNum= function (){
            return this.num;
        }
        this.getStart= function (){
            return this.start;
        }
        this.setStart= function (st){
            this.start=st;
        }
    }
    let year=new Year(y,-1);
    initial=3;
    let cur=1000;
    let flag=false;
    while(flag==false){
        if(cur>year.getNum()){
            if(initial==year.getStart()){
                if(!(cur%400==0||(cur%4==0&&cur%100!=0))&&!(year.getNum()%400==0||(year.getNum()%4==0&&year.getNum()%100!=0))){
                    flag=true;
                }
                else if((cur%400==0||(cur%4==0&&cur%100!=0))&&(year.getNum()%400==0||(year.getNum()%4==0&&year.getNum()%100!=0))){
                    flag=true;
                }
            }
        }
        else if(cur==year.getNum()){
            year.setStart(initial);
        }
        if(cur%400==0||(cur%4==0&&cur%100!=0)){
            initial+=2;
            initial=initial%7;
        }
        else{
            initial++;
            initial=initial%7;
        }
        cur++;
    }
    document.write("El siguiente año que empieza y termina el mismo día de la semana es: ")
    document.write(cur-1);
}
calendario();