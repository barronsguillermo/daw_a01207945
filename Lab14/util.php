<?php
    function connectDb(){
        $servername="localhost";
        $username="root";
        $password="";
        $dbname="examen";
        $con=mysqli_connect($servername,$username,$password,$dbname);
        if(!$con){
            die("Connection failed: ".mysqli_connect_error());
        }
        return $con;
    }
    function closeDb($con){
        mysqli_close($con);
    }
    function getExam(){
        $conn=connectDb();
        $sql="SELECT e.idExamen as ide, a.Tipo as tip, a.Valor as val FROM Examen e, Atributo a WHERE e.idExamen=a.idExamen";
        $result=mysqli_query($conn, $sql);
        closeDb($conn);
        return $result;
    }
?>