<?php
    $json_url = "https://newton.now.sh/".$_POST["operation"]."/".$_POST["expression"]."";
    $ch = curl_init( $json_url );
    $options = array(
        CURLOPT_RETURNTRANSFER => true,
        //CURLOPT_USERPWD	=> $username . “:” . $password,  // authentication
        //CURLOPT_HTTPHEADER => array(‘Content-type: application/json’) ,
        //CURLOPT_POSTFIELDS => $json_string
    );
    curl_setopt_array( $ch, $options );
    $result = curl_exec($ch);
    $array=json_decode($result,true);
    //var_dump($array);
    echo "<header>Result</header><br>";
    if(array_key_exists("error",$array)){
        echo "error";
    }
    else{
        if(count($array["result"])==1){
            echo $array["result"];
        }
        else{
            $i=0;
            while($i<count($array["result"])){
                echo $array["result"][$i]." ";
                $i=$i+1;
            }
        }
    }
?>