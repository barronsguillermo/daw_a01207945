<?php
    require_once("util.php");
    $conn=connectDb();
    if($conn->connect_errno){
        echo "Falló la conexión.";
    }
    if(!$conn->query("DROP PROCEDURE IF EXISTS p") || !$conn->query("CREATE PROCEDURE p() READS SQL DATA BEGIN SELECT NombrePaciente FROM Paciente; END;")){
        echo "Falló creación procedimiento";
    }
    if (!$conn->multi_query("CALL p()")){
        echo "Falló CALL: (" . $conn->errno . ") " . $conn->error;
    }
    do{
        if($resultado=$conn->store_result()){
            printf("---\n");
            var_dump($resultado->fetch_all());
            $resultado->free();
        }
        else{
            if ($conn->errno) {
                echo "Store failed: (" . $conn->errno . ") " . $conn->error;
            }
        }
    }while ($conn->more_results() && $conn->next_result());
    closeDb($conn);
?>