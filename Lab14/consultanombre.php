<?php
    function connectDb(){
        $servername="localhost";
        $username="root";
        $password="";
        $dbname="examen";
        $con=mysqli_connect($servername,$username,$password,$dbname);
        if(!$con){
            die("Connection failed: ".mysqli_connect_error());
        }
        return $con;
    }
    function closeDb($con){
        mysqli_close($con);
    }
    function getExamsByName($name){
        $conn=connectDb();
        $sql="SELECT a.Tipo as tip, a.Valor as val FROM Examen e, Atributo a,Paciente p WHERE e.idExamen=a.idExamen AND e.idPaciente=p.idPaciente AND p.nombrePaciente='".$name."'";
        $result=mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
    }
?>