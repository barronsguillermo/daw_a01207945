SET DATEFORMAT DMY

BULK INSERT a1207945.a1207945.[Materiales] 
  FROM 'e:\wwwroot\a1207945\materiales.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT a1207945.a1207945.[Proyectos] 
  FROM 'e:\wwwroot\a1207945\Proyectos.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT a1207945.a1207945.[Proveedores] 
  FROM 'e:\wwwroot\a1207945\Proveedores.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT a1207945.a1207945.[Entregan] 
  FROM 'e:\wwwroot\a1207945\Entregan.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 