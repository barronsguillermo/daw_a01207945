function verificar(){
    let n=document.getElementById("pass1").value;
    let m=document.getElementById("pass2").value;
    if(n===m){
        window.alert("Coincidencia, correcto.");
    }
    else{
        document.getElementById("pass1").value=" ";
        document.getElementById("pass2").value=" ";
        window.alert("No hay coincidencia, intentalo de nuevo.");
    }
}
document.getElementById("Verifica").onclick=verificar;
function actualizarPrecio1(){
    let prod=Number(document.getElementById("num1").innerHTML.substr(20,document.getElementById("num1").innerHTML.length-20));
    let cost=Number(document.getElementById("precio1").innerHTML.substr(9,document.getElementById("precio1").innerHTML.length-8));
    let precio=Number(document.getElementById("costototal").innerHTML.substr(15,document.getElementById("costototal").innerHTML.length));
    if(prod<100){
        prod++;
        document.getElementById("num1").innerHTML="Productos a llevar: "+prod;
        precio=precio+cost;
     document.getElementById("costototal").innerHTML="Precio total: $"+precio;
        document.getElementById("IVA").innerHTML="IVA: $"+(precio*0.15);
    }
}
document.getElementById("agregar1").onclick=actualizarPrecio1;
function actualizarPrecio2(){
    let prod=Number(document.getElementById("num2").innerHTML.substr(20,document.getElementById("num2").innerHTML.length-20));
    let cost=Number(document.getElementById("precio2").innerHTML.substr(9,document.getElementById("precio2").innerHTML.length-8));
    let precio=Number(document.getElementById("costototal").innerHTML.substr(15,document.getElementById("costototal").innerHTML.length));
    if(prod<100){
        prod++;
        document.getElementById("num2").innerHTML="Productos a llevar: "+prod;
        precio=precio+cost;
        document.getElementById("costototal").innerHTML="Precio total: $"+precio;
        document.getElementById("IVA").innerHTML="IVA: $"+(precio*0.15);
    }
}
document.getElementById("agregar2").onclick=actualizarPrecio2;
function actualizarPrecio3(){
    let prod=Number(document.getElementById("num3").innerHTML.substr(20,document.getElementById("num3").innerHTML.length-20));
    let cost=Number(document.getElementById("precio3").innerHTML.substr(9,document.getElementById("precio3").innerHTML.length-8));
    let precio=Number(document.getElementById("costototal").innerHTML.substr(15,document.getElementById("costototal").innerHTML.length));
    if(prod<100){
        prod++;
        document.getElementById("num3").innerHTML="Productos a llevar: "+prod;
        precio=precio+cost;
        document.getElementById("costototal").innerHTML="Precio total: $"+precio;
        document.getElementById("IVA").innerHTML="IVA: $"+(precio*0.15);
    }
}
document.getElementById("agregar3").onclick=actualizarPrecio3;
function revisar(){
    if(Number(document.getElementById("x1").value)<-1||Number(document.getElementById("x1").value)>11){
        window.alert("Coordenada X 1 fuera de rango");
    }
    else if(Number(document.getElementById("y1").value)<-1||Number(document.getElementById("y1").value)>7){
        window.alert("Coordenada Y 1 fuera de rango");
    }
    else if(Number(document.getElementById("x2").value)<-1||Number(document.getElementById("x2").value)>11){
        window.alert("Coordenada X 2 fuera de rango");
    }
    else if(Number(document.getElementById("y2").value)<-1||Number(document.getElementById("y2").value)>7){
        window.alert("Coordenada Y 2 fuera de rango");
    }
    else{
        document.getElementById("ans").innerHTML="La distancia de: "+document.getElementById("nom1").value+" a "+document.getElementById("nom2").value+" es aproximadamente: "+Math.sqrt(Math.pow(Number(document.getElementById("x1").value)-Number(document.getElementById("x2").value),2)+Math.pow(Number(document.getElementById("y1").value)-Number(document.getElementById("y2").value),2))+" unidades.";
    }
}
document.getElementById("sub").onclick=revisar;