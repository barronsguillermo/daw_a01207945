-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-10-2017 a las 04:39:13
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `p` (OUT `msg` VARCHAR(50))  BEGIN SELECT NombrePaciente FROM paciente WHERE idPaciente=1 INTO msg; END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `atributo`
--

CREATE TABLE `atributo` (
  `idAtributo` int(6) NOT NULL,
  `idExamen` int(6) NOT NULL,
  `Tipo` varchar(20) COLLATE utf8_bin NOT NULL,
  `Valor` varchar(40) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `atributo`
--

INSERT INTO `atributo` (`idAtributo`, `idExamen`, `Tipo`, `Valor`) VALUES
(1, 1, 'Talla', '167'),
(2, 1, 'Peso', '70'),
(3, 1, 'TipoSangre', 'A+'),
(4, 1, 'LEU', '1'),
(5, 1, 'NIT', '1'),
(6, 1, 'URO', '3'),
(7, 1, 'PRO', '2'),
(8, 2, 'Talla', '165'),
(9, 2, 'Peso', '77'),
(10, 2, 'TipoSangre', 'B-'),
(11, 2, 'LEU', '2'),
(12, 2, 'NIT', '1'),
(13, 2, 'URO', '1'),
(14, 2, 'PRO', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen`
--

CREATE TABLE `examen` (
  `idExamen` int(6) NOT NULL,
  `idPaciente` int(6) NOT NULL,
  `Fecha` date NOT NULL,
  `Tipo` varchar(40) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `examen`
--

INSERT INTO `examen` (`idExamen`, `idPaciente`, `Fecha`, `Tipo`) VALUES
(1, 1, '2017-10-01', 'Urianlisis'),
(2, 2, '2017-09-04', 'Urianalisis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `idPaciente` int(6) NOT NULL,
  `NombrePaciente` varchar(50) COLLATE utf8_bin NOT NULL,
  `FechaNacimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`idPaciente`, `NombrePaciente`, `FechaNacimiento`) VALUES
(1, 'Jorge Garrido', '1987-10-09'),
(3, 'Adan Pena', '1991-01-01'),
(4, 'Rodrigo Perez', '1991-01-01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`idPaciente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `paciente`
--
ALTER TABLE `paciente`
  MODIFY `idPaciente` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
