BULK INSERT a1207945.a1207945.[Proveedores]
	FROM 'e:\wwwroot\a1207945\proveedores.csv'
	WITH
	(
		CODEPAGE = 'ACP',
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n'
	)