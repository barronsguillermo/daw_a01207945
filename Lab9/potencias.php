<!DOCTYPE html>
<html>
    <head>
        <title>Lab 9</title>
        <meta charset="utf-8">
    </head>
    <body>
        <header><h1>Respuesta Problema 4</h1></header>
        <?php
            $n=$_GET["n4"];
            echo "<table><thead><tr><td>Número</td><td>Cuadrado</td><td>Cubo</td></tr></thead><tbody>";
            $i=0;
            for($i=1;$i<=$_GET["n4"];$i++){
                echo "<tr><td>".$i."</td><td>".($i*$i)."</td><td>".($i*$i*$i)."</td></tr>";
            }
            echo "</tbody></table>";
        ?>
    </body>
</html>