<?php
    function connectDb(){
        $servername="localhost";
        $username="root";
        $password="";
        $dbname="examen";
        $con=mysqli_connect($servername,$username,$password,$dbname);
        if(!$con){
            die("Connection failed: ".mysqli_connect_error());
        }
        return $con;
    }
    function closeDb($con){
        mysqli_close($con);
    }
    function getExam(){
        $conn=connectDb();
        $sql="SELECT e.idExamen as ide, a.Tipo as tip, a.Valor as val FROM Examen e, Atributo a WHERE e.idExamen=a.idExamen";
        $result=mysqli_query($conn, $sql);
        closeDb($conn);
        return $result;
    }

    function getPatients(){
        $conn=connectDb();
        $sql="SELECT * FROM Paciente p";
        $result=mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
    }

    function getPat($id){
        $conn=connectDb();
        $sql="SELECT * FROM Paciente p WHERE p.idPaciente=".$id;
        $result=mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
    }
    
    function insert_patient($name,$fecha){
        // insert command specification 
        $conn=connectDb();
        $query='INSERT INTO paciente (NombrePaciente,FechaNacimiento) VALUES (?,?) ';
        // Preparing the statement 
        if (!($statement = $conn->prepare($query))) {
            die("Preparation failed: (" . $conn->errno . ") " . $conn->error);
        }
        else{
        }
        // Binding statement params 
        if (!$statement->bind_param("ss", $name, $fecha)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        else{
        }
         // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }
        closeDb($conn);
    }

    function update_patient($name,$fecha, $id){
        // update command specification
        $conn=connectDb();
        $query="UPDATE paciente SET NombrePaciente=? WHERE idPaciente=?";
        echo $id;
        if (!($statement = $conn->prepare($query))) {
            die("The preparation failed: (" . $conn->errno . ") " . $con->error);
        }
        // Binding statement params
        $description='Modified description';
        $upc='7777788888';
        if (!$statement->bind_param("ss", $name, $id)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        } 
        // update execution
        if ($statement->execute()) {
            echo 'There were ' . mysqli_affected_rows($conn) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
        closeDb($conn);

    }

    function delete_patient($id){
        $conn=connectDb();
        // Deletion query construction
        $query= "DELETE FROM Paciente WHERE idPaciente=?";
            if (!($statement = $conn->prepare($query))) {
                die("The preparation failed: (" . $conn->errno . ") " . $conn->error);
            }
            // Binding statement params
            $upc='7777788888';
            if (!$statement->bind_param("s", $id)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
            } 
            // delete execution
            if ($statement->execute()) {
                echo 'There were ' . mysqli_affected_rows($conn) . ' affected rows';
            } else {
                die("Update failed: (" . $statement->errno . ") " . $statement->error);
            }

        closeDb($conn);
    }
?>